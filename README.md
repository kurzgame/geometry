# geometry (v1.1.7 beta)

Module to easily handle geometry objects with integer precision only.

## Integer

This lib was designed to work with 2D shapes on image surface (ImageData).

So all positions/rectangles/e.t.c. are should have integer position and integer size to avoid ambiguous situations with floating point.

The fact that any geometry object is not contains any floating point - will reduce a lot of unneeded casts and checks.

Also, integer math is much easier!

But! During time I've decided to implement support for floating point too, when all features will be done and tested well for integer case.

## Immutability

Each constructed geometry object is immutable.

This provides safety and stability to the code. Unfortunately, this means performance issues since you can't just change the value (let's say x-coordinateof the Point instance), instead you have to create the whole new object from scratch.

So consider this before using this lib.

## Usage

 * To check everything is OK - execute command ```npm run test```
 * To build library for front-end usage - execute command ```npm run build```

Code example:

```javascript
// Triangle-like polygon:
var poly = new geometry.Polygon([
    new geometry.Point(0, 0),
    new geometry.Point(4, 4),
    new geometry.Point(0, 4)]);
var boundingRect = poly.getBoundingRectangle();
// Check for intersection:
console.log(boundingRect.intersects(new geometry.Line(
    new geometry.Point(6, 1),
    new geometry.Point(0, 2)))); // true
```

## Browser Capability

Should work well almost everywhere (requires ECMAScript5 support).

## Feedback

For any questions/propositions/e.t.c you can contact me at <kurzgame@gmail.com>
