var geometry = require("../src/index");
var expect = require("chai").expect;

describe("IShape", function() {
    describe("new IShape", function () {
        it("creates abstract view object", function () {
            var shape = new geometry.IShape;
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var shape = new geometry.IShape;
            expect(shape.toString()).to.equal("[object IShape]");
        });
    });

    describe("constructor", function () {
        it("checks constructor property", function () {
            var shape = new geometry.IShape;
            expect(shape.constructor).to.equal(geometry.IShape);
        });
    });

    describe("null methods", function () {
        it("checks 'virtual' methods", function () {
            var shape = new geometry.IShape;

            expect(shape.equals).to.equal(null);
            expect(shape.contains).to.equal(null);
            expect(shape.intersects).to.equal(null);
        });
    });
});
