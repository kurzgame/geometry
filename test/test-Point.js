var geometry = require("../src/index");
var expect = require("chai").expect;

describe("Point", function(){
    describe("new Point", function(){
        it("creates point object", function(){
            var point = new geometry.Point;
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var point = new geometry.Point;
            expect(point.toString()).to.equal("[object Point]");
        });
    });

    describe("constructor", function () {
        it("checks constructor property", function () {
            var point = new geometry.Point;
            expect(point.constructor).to.equal(geometry.Point);
        });
    });

    describe("new Point", function(){
        it("creates point object from properties", function(){
            var point = new geometry.Point(1, 2);

            expect(point.x).to.equal(1);
            expect(point.y).to.equal(2);
        });

        it("creates point object from wrong properties", function(){
            var point = new geometry.Point(3.14, 9.8); // floats are currently not allowed!

            expect(point.x).to.equal(3);
            expect(point.y).to.equal(9);
        });

        it("creates point object from NaN", function(){
            var point = new geometry.Point(NaN, -NaN);

            expect(point.x).to.equal(0);
            expect(point.y).to.equal(0);
        });

        it("creates point object from wrong properties", function(){
            var point = new geometry.Point(false, "3.14");

            expect(point.x).to.equal(0);
            expect(point.y).to.equal(3);
        });

        it("creates point object from Infinity", function(){
            var point = new geometry.Point(Infinity, -Infinity);

            expect(point.x).to.equal(0);
            expect(point.y).to.equal(0);
        });
    });

    describe("equals()", function(){
        it("checks two same points", function(){
            var point1 = new geometry.Point(1, 2);
            var point2 = new geometry.Point(1, 2);

            expect(point1.equals(point2)).to.equal(true);
        });

        it("checks point instance", function(){
            var point = new geometry.Point(1, 2);

            expect(point.equals(point)).to.equal(true);
        });

        it("checks two different points", function(){
            var point1 = new geometry.Point(1, 2);
            var point2 = new geometry.Point(2, 1);

            expect(point1.equals(point2)).to.equal(false);
        });
    });

    describe("getDistance()", function(){
        it("get distance between points that lies on same y-axis", function(){
            var a = new geometry.Point(0, 3);
            var b = new geometry.Point(4, 3);

            var result = a.getDistance(b);

            expect(result).to.equal(4);
        });

        it("get distance between points that lies on same x-axis", function(){
            var a = new geometry.Point(4, 5);
            var b = new geometry.Point(4, 3);

            var result = a.getDistance(b);

            expect(result).to.equal(2);
        });

        it("get distance between points", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);

            var result = a.getDistance(b);

            expect(result).to.equal(Math.sqrt(18)); // sqrt(3 * 3 + 3 * 3)
        });

        it("get distance between points", function(){
            var a = new geometry.Point(-1, 1);
            var b = new geometry.Point(3, 3);

            var result = a.getDistance(b);

            expect(result).to.equal(Math.sqrt(4 * 4 + 2 * 2));
        });

        it("get distance between points", function(){
            var a = new geometry.Point(2, 1);
            var b = new geometry.Point(3, -3);

            var result = a.getDistance(b);

            expect(result).to.equal(Math.sqrt(1 + 4 * 4));
        });

        it("get distance between points", function(){
            var a = new geometry.Point(-2, -1);
            var b = new geometry.Point(-5, -3);

            var result = a.getDistance(b);

            expect(result).to.equal(Math.sqrt(3 * 3 + 2 * 2));
        });
    });
    
    describe("getSlope()", function(){
        it("checks two points at 45 degree", function(){
            var point1 = new geometry.Point(0, 4);
            var point2 = new geometry.Point(4, 0);

            expect(point1.getSlope(point2)).to.equal(-1);
        });

        it("checks two points at 45 degree", function(){
            var point1 = new geometry.Point(4, 0);
            var point2 = new geometry.Point(0, 4);

            expect(point1.getSlope(point2)).to.equal(-1);
        });

        it("checks two horizontal points", function(){
            var point1 = new geometry.Point(4, 1);
            var point2 = new geometry.Point(8, 1);

            expect(point1.getSlope(point2)).to.equal(0);
        });

        it("checks two horizontal points", function(){
            var point1 = new geometry.Point(8, 1);
            var point2 = new geometry.Point(4, 1);

            expect(point1.getSlope(point2)).to.equal(0);
        });

        it("checks two vertical points", function(){
            var point1 = new geometry.Point(1, 4);
            var point2 = new geometry.Point(1, 8);

            expect(point1.getSlope(point2)).to.not.be.finite;
        });

        it("checks two vertical points", function(){
            var point1 = new geometry.Point(1, 8);
            var point2 = new geometry.Point(1, 4);

            expect(point1.getSlope(point2)).to.not.be.finite;
        });

        it("checks two same points", function(){
            var point1 = new geometry.Point(3, 2);
            var point2 = new geometry.Point(3, 2);

            expect(point1.getSlope(point2)).to.be.NaN;
        });

    });

});
