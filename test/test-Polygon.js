var geometry = require("../src/index");
var expect = require("chai").expect;

describe("Polygon", function(){
    describe("new Polygon", function(){
        it("creates point object", function(){
            var poly = new geometry.Polygon([]);

            expect(poly instanceof geometry.IShape).to.equal(true);
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var poly = new geometry.Polygon([]);
            expect(poly.toString()).to.equal("[object Polygon]");
        });
    });

    describe("constructor", function () {
        it("checks constructor property", function () {
            var poly = new geometry.Polygon([]);
            expect(poly.constructor).to.equal(geometry.Polygon);
        });
    });

    describe("contains()", function(){
        it("checks point inside the polygon (point is between two vertices (horizontally))", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 5),
                new geometry.Point(1, 3),
                new geometry.Point(4, 0),
                new geometry.Point(5, 3),
                new geometry.Point(4, 5),
                new geometry.Point(3, 4)
            ]);

            expect(poly.contains(new geometry.Point(3, 3))).to.equal(true);
        });

        it("checks point inside the polygon (point is between three vertices (horizontally))", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 5),
                new geometry.Point(1, 3),
                new geometry.Point(3, 3),
                new geometry.Point(4, 0),
                new geometry.Point(5, 3),
                new geometry.Point(4, 5),
                new geometry.Point(3, 4)
            ]);

            expect(poly.contains(new geometry.Point(4, 3))).to.equal(true);
        });

        it("checks point outside complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 8),
                new geometry.Point(3, 1),
                new geometry.Point(9, 1),
                new geometry.Point(9, 9),
                new geometry.Point(5, 9),
                new geometry.Point(3, 3),
                new geometry.Point(8, 4),
                new geometry.Point(8, 2),
                new geometry.Point(4, 2),
                new geometry.Point(6, 7)
            ]);

            expect(poly.contains(new geometry.Point(4, 4))).to.equal(false);
            expect(poly.contains(new geometry.Point(4, 5))).to.equal(false);
            expect(poly.contains(new geometry.Point(5, 5))).to.equal(false);
            expect(poly.contains(new geometry.Point(5, 6))).to.equal(false);
            
            expect(poly.contains(new geometry.Point(5, 3))).to.equal(false);
            expect(poly.contains(new geometry.Point(6, 3))).to.equal(false);
            expect(poly.contains(new geometry.Point(7, 3))).to.equal(false);
        });

        it("checks all points inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 0),
                new geometry.Point(2, 2),
                new geometry.Point(4, 2),
                new geometry.Point(5, 1)
            ]);

            expect(poly.contains(new geometry.Point(1, 0))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 1))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 2))).to.equal(true);
        });

        it("checks all points of the arbitrary polygon to be inside it", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(7, 1),
                new geometry.Point(1, 2),
                new geometry.Point(5, 4),
                new geometry.Point(2, 7),
                new geometry.Point(8, 7),
                new geometry.Point(9, 2)
            ]);

            expect(poly.contains(new geometry.Point(7, 1))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(9, 2))).to.equal(true);

            expect(poly.contains(new geometry.Point(3, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 3))).to.equal(true);

            expect(poly.contains(new geometry.Point(5, 4))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 4))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 4))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 4))).to.equal(true);

            expect(poly.contains(new geometry.Point(4, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 5))).to.equal(true);

            expect(poly.contains(new geometry.Point(3, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 6))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(7, 7))).to.equal(true);
            expect(poly.contains(new geometry.Point(8, 7))).to.equal(true);
        });

        it("checks all points of the complex arbitrary polygon to be inside it", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            expect(poly.contains(new geometry.Point(1, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(2, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 1))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 2))).to.equal(true);

            expect(poly.contains(new geometry.Point(3, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 3))).to.equal(true);

            expect(poly.contains(new geometry.Point(3, 4))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 4))).to.equal(true);

            expect(poly.contains(new geometry.Point(2, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 5))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 5))).to.equal(true);

            expect(poly.contains(new geometry.Point(1, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(2, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(4, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(5, 6))).to.equal(true);
            expect(poly.contains(new geometry.Point(6, 6))).to.equal(true);

        });

        it("checks point to be outside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 0),
                new geometry.Point(2, 2),
                new geometry.Point(4, 2),
                new geometry.Point(5, 1)
            ]);

            expect(poly.contains(new geometry.Point(1, 2))).to.equal(false);
            expect(poly.contains(new geometry.Point(4, 0))).to.equal(false);
            expect(poly.contains(new geometry.Point(3, 0))).to.equal(false);
        });

        it("checks point that stands exact on polygon's edges", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(3, 3),
                new geometry.Point(0, 3)
            ]);

            expect(poly.contains(new geometry.Point(0, 0))).to.equal(true);
            expect(poly.contains(new geometry.Point(1, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(2, 2))).to.equal(true);
            expect(poly.contains(new geometry.Point(3, 3))).to.equal(true);

            expect(poly.contains(new geometry.Point(0, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(1, 3))).to.equal(true);
            expect(poly.contains(new geometry.Point(2, 3))).to.equal(true);

            expect(poly.contains(new geometry.Point(0, 1))).to.equal(true);
            expect(poly.contains(new geometry.Point(0, 2))).to.equal(true);
        });

        it("checks line to be inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var line = new geometry.Line(
                new geometry.Point(3, 3),
                new geometry.Point(2, 3));

            expect(poly.contains(line)).to.equal(true);
        });

        it("checks line to be inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var line = new geometry.Line(
                new geometry.Point(1, 1),
                new geometry.Point(1, 4));

            expect(poly.contains(line)).to.equal(true);
        });

        it("checks line to be inside the arbitrary polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(1, 7),
                new geometry.Point(7, 7),
                new geometry.Point(7, 1),
                new geometry.Point(4, 5)
            ]);

            var line = new geometry.Line(
                new geometry.Point(2, 6),
                new geometry.Point(6, 6));

            expect(poly.contains(line)).to.equal(true);
        });

        it("checks line to be inside the arbitrary polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(1, 7),
                new geometry.Point(7, 7),
                new geometry.Point(7, 1),
                new geometry.Point(4, 5)
            ]);

            var line = new geometry.Line(
                new geometry.Point(2, 4),
                new geometry.Point(6, 4));

            expect(poly.contains(line)).to.equal(false);
        });

        it("checks line to be inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 0),
                new geometry.Point(2, 2),
                new geometry.Point(4, 2),
                new geometry.Point(5, 1)
            ]);

            var line = new geometry.Line(
                new geometry.Point(1, 0),
                new geometry.Point(4, 1));

            expect(poly.contains(line)).to.equal(true);
        });

        it("checks line to be outside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 0),
                new geometry.Point(2, 2),
                new geometry.Point(4, 2),
                new geometry.Point(5, 1)
            ]);

            var line = new geometry.Line(
                new geometry.Point(2, 1),
                new geometry.Point(3, 3));

            expect(poly.contains(line)).to.equal(false);
        });

        it("checks polygon to be inside the polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(3, 3),
                new geometry.Point(1, 3)
            ]);

            expect(poly1.contains(poly2)).to.equal(true);
        });

        it("checks polygon not to be inside the convex polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(1, 7),
                new geometry.Point(7, 7),
                new geometry.Point(7, 1),
                new geometry.Point(4, 5)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(2, 6),
                new geometry.Point(6, 6),
                new geometry.Point(6, 4)
            ]);

            expect(poly1.contains(poly2)).to.equal(false);
        });

        it("checks polygon to be outside the polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(4, 1),
                new geometry.Point(1, 3)
            ]);

            expect(poly1.contains(poly2)).to.equal(false);
        });

        it("checks rectangle to be inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(4, 0),
                new geometry.Point(8, 5),
                new geometry.Point(0, 4)
            ]);

            var rect = new geometry.Rectangle(3, 2, 2, 1);

            expect(poly.contains(rect)).to.equal(true);
        });

        it("checks rectangle not to be inside convex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(1, 7),
                new geometry.Point(7, 7),
                new geometry.Point(7, 1),
                new geometry.Point(4, 5)
            ]);

            var rect = new geometry.Rectangle(2, 4, 5, 2);

            expect(poly.contains(rect)).to.equal(false);
        });

        it("checks rectangle to be not inside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(1, 5),
                new geometry.Point(5, 5),
                new geometry.Point(5, 1)
            ]);

            var rect = new geometry.Rectangle(1, 1, 4, 4);

            expect(poly.contains(rect)).to.equal(true);
        });

        it("checks rectangle to be outside the polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(4, 0),
                new geometry.Point(8, 5),
                new geometry.Point(0, 4)
            ]);

            var rect = new geometry.Rectangle(0, 3, 4, 1);

            expect(poly.contains(rect)).to.equal(false);
        });
    });

    describe("equals()", function(){
        it("checks two same polygons", function(){
            var poly1 = new geometry.Polygon([]);
            var poly2 = new geometry.Polygon([]);

            expect(poly1.equals(poly2)).to.equal(true);
        });

        it("checks two same polygons", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);
            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);

            expect(poly1.equals(poly2)).to.equal(true);
        });

        it("checks two different polygons", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);
            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);

            expect(poly1.equals(poly2)).to.equal(false);
        });

        it("checks two almost same polygons (order of vertices is different)", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);
            var poly2 = new geometry.Polygon([
                new geometry.Point(2, 2),
                new geometry.Point(0, 1),
                new geometry.Point(3, 3)
            ]);

            expect(poly1.equals(poly2)).to.equal(false);
        });

        it("checks two polygons with different amount of vertices", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(2, 2),
                new geometry.Point(3, 3)
            ]);
            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(2, 2)
            ]);

            expect(poly1.equals(poly2)).to.equal(false);
        });
    });

    describe("intersects()", function(){
        it("check point intersection with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            expect(poly.intersects(new geometry.Point(0, 2))).to.equal(true);
            expect(poly.intersects(new geometry.Point(3, 3))).to.equal(true);
        });

        it("check point to not intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            expect(poly.intersects(new geometry.Point(-1, 2))).to.equal(false);
        });

        it("check line to intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var line = new geometry.Line(
                new geometry.Point(1, 2),
                new geometry.Point(3, 0));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var line = new geometry.Line(
                new geometry.Point(2, 5),
                new geometry.Point(3, 0));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(6, 0),
                new geometry.Point(6, 7));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(1, 0),
                new geometry.Point(1, 7));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(7, 0),
                new geometry.Point(0, 7));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(0, 0),
                new geometry.Point(7, 7));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(0, 1),
                new geometry.Point(7, 1));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to intersects with complex polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(6, 1),
                new geometry.Point(1, 6),
                new geometry.Point(6, 6)
            ]);

            var line = new geometry.Line(
                new geometry.Point(0, 6),
                new geometry.Point(7, 6));

            expect(poly.intersects(line)).to.equal(true);
        });

        it("check line to not intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var line = new geometry.Line(
                new geometry.Point(3, 1),
                new geometry.Point(3, 0));

            expect(poly.intersects(line)).to.equal(false);
        });

        it("check polygon to intersects with polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(3, 3),
                new geometry.Point(5, 0),
                new geometry.Point(6, 1)
            ]);

            expect(poly1.intersects(poly2)).to.equal(true);
        });

        it("check polygon to intersects with polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(-2, 2),
                new geometry.Point(5, 0),
                new geometry.Point(6, 1)
            ]);

            expect(poly1.intersects(poly2)).to.equal(true);
        });

        it("check polygon to contain another polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(7, 7),
                new geometry.Point(0, 7)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(5, 5),
                new geometry.Point(0, 5)
            ]);

            expect(poly1.intersects(poly2)).to.equal(true);
        });

        it("check polygon to not intersects with polygon", function(){
            var poly1 = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var poly2 = new geometry.Polygon([
                new geometry.Point(3, 0),
                new geometry.Point(5, 0),
                new geometry.Point(6, 1)
            ]);

            expect(poly1.intersects(poly2)).to.equal(false);
        });

        it("check rectangle to intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var rect = new geometry.Rectangle(0, 3, 4, 1);

            expect(poly.intersects(rect)).to.equal(true);
        });

        it("check rectangle to not intersects with polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 4),
                new geometry.Point(0, 4)
            ]);

            var rect = new geometry.Rectangle(3, -1, 2, 2);

            expect(poly.intersects(rect)).to.equal(false);
        });

        it("check rectangle to not intersects with empty polygon", function(){
            var poly = new geometry.Polygon([]);

            var rect = new geometry.Rectangle(0, 0, 2, 2);

            expect(poly.intersects(rect)).to.equal(false);
        });
    });

    describe("getBoundingRectangle()", function(){
        it("bounding rectangle of empty polygon", function(){
            var poly = new geometry.Polygon([]);

            var rect = poly.getBoundingRectangle();

            expect(rect).to.equal(null);
        });

        it("bounding rectangle of polygon with only one point", function(){
            var poly = new geometry.Polygon([new geometry.Point(4, 5)]);

            var rect = poly.getBoundingRectangle();

            expect(rect.x).to.equal(4);
            expect(rect.y).to.equal(5);
            expect(rect.width).to.equal(1);
            expect(rect.height).to.equal(1);
            expect(rect.contains(poly)).to.equal(true);
        });

        it("bounding rectangle of rectangle-like polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(10, 0),
                new geometry.Point(10, 10),
                new geometry.Point(0, 10)
            ]);

            var rect = poly.getBoundingRectangle();

            expect(rect.x).to.equal(0);
            expect(rect.y).to.equal(0);
            expect(rect.width).to.equal(10);
            expect(rect.height).to.equal(10);

            expect(rect.contains(poly)).to.equal(true);
            expect(poly.contains(rect)).to.equal(true);
        });

        it("bounding rectangle of polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(8, 0),
                new geometry.Point(7, 4),
                new geometry.Point(4, 5),
                new geometry.Point(2, 3)
            ]);

            var rect = poly.getBoundingRectangle();

            expect(rect.x).to.equal(1);
            expect(rect.y).to.equal(0);
            expect(rect.width).to.equal(7);
            expect(rect.height).to.equal(5);
            
            expect(rect.contains(poly)).to.equal(true);
        });

    });

    describe("clip()", function(){
        it("clips line inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(4, 1), new geometry.Point(4, 7));

            var result = poly.clip(line);

            expect(result.length).to.equal(2);

            expect(result[0].x).to.equal(4);
            expect(result[0].y).to.equal(2);

            expect(result[1].x).to.equal(4);
            expect(result[1].y).to.equal(6);
        });

        it("clips line inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(1, 5), new geometry.Point(8, 5));

            var result = poly.clip(line);

            expect(result.length).to.equal(2);

            expect(result[0].x).to.equal(6);
            expect(result[0].y).to.equal(5);

            expect(result[1].x).to.equal(3);
            expect(result[1].y).to.equal(5);
        });

        it("clips line inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(2, 1),
                new geometry.Point(2, 7),
                new geometry.Point(7, 9),
                new geometry.Point(9, 1),
                new geometry.Point(5, 5)]);
            var line = new geometry.Line(new geometry.Point(1, 3), new geometry.Point(10, 3));

            var result = poly.clip(line);

            expect(result.length).to.equal(4);

            expect(result[0].x).to.equal(2);
            expect(result[0].y).to.equal(3);

            expect(result[1].x).to.equal(4);
            expect(result[1].y).to.equal(3);

            expect(result[2].x).to.equal(7);
            expect(result[2].y).to.equal(3);

            expect(result[3].x).to.equal(9);
            expect(result[3].y).to.equal(3);
        });

        it("clips line inside polygon (one point is inside of polygon)", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(1, 5), new geometry.Point(5, 5));

            var result = poly.clip(line);

            expect(result.length).to.equal(2);

            expect(result[0].x).to.equal(5);
            expect(result[0].y).to.equal(5);

            expect(result[1].x).to.equal(3);
            expect(result[1].y).to.equal(5);
        });

        it("clips line fully inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(4, 3), new geometry.Point(5, 5));

            var result = poly.clip(line);

            expect(result.length).to.equal(2);

            expect(result[0].x).to.equal(4);
            expect(result[0].y).to.equal(3);

            expect(result[1].x).to.equal(5);
            expect(result[1].y).to.equal(5);
        });

        it("tries to clip line outside of polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(1, 1), new geometry.Point(1, 5));

            var result = poly.clip(line);

            expect(result.length).to.equal(0);
        });

        it("tries to clip line outside of polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(2, 4),
                new geometry.Point(6, 7)]);
            var line = new geometry.Line(new geometry.Point(1, -1), new geometry.Point(7, 0));

            var result = poly.clip(line);

            expect(result.length).to.equal(0);
        });

        /*it("clips one polygon against another polygon", function(){
            // TODO
        });*/

        it("clips rectangle inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(3, 1),
                new geometry.Point(1, 5),
                new geometry.Point(6, 6)]);
            var rect = new geometry.Rectangle(1, 3, 5, 2);

            var result = poly.clip(rect);

            expect(result.length).to.equal(4);

            expect(result[0].x).to.equal(4);
            expect(result[0].y).to.equal(3);

            expect(result[1].x).to.equal(2);
            expect(result[1].y).to.equal(3);

            expect(result[2].x).to.equal(1);
            expect(result[2].y).to.equal(5);

            expect(result[3].x).to.equal(5);
            expect(result[3].y).to.equal(5);
        });

        it("clips rectangle inside polygon (only one rect vertex inside polygon)", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(3, 1),
                new geometry.Point(1, 5),
                new geometry.Point(6, 6)]);
            var rect = new geometry.Rectangle(4, 4, 5, 4);

            var result = poly.clip(rect);

            expect(result.length).to.equal(3);

            expect(result[0].x).to.equal(4);
            expect(result[0].y).to.equal(4);

            expect(result[1].x).to.equal(4);
            expect(result[1].y).to.equal(5);

            expect(result[2].x).to.equal(6);
            expect(result[2].y).to.equal(6);
        });

        it("clips rectangle inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(3, 2),
                new geometry.Point(6, 5),
                new geometry.Point(2, 7),
                new geometry.Point(7, 7),
                new geometry.Point(8, 2)]);
            var rect = new geometry.Rectangle(4, 1, 3, 6);

            var result = poly.clip(rect);

            expect(result.length).to.equal(7);

            expect(result[0].x).to.equal(7);
            expect(result[0].y).to.equal(7);

            expect(result[1].x).to.equal(7);
            expect(result[1].y).to.equal(2);

            expect(result[2].x).to.equal(4);
            expect(result[2].y).to.equal(2);

            expect(result[3].x).to.equal(4);
            expect(result[3].y).to.equal(3);

            expect(result[4].x).to.equal(6);
            expect(result[4].y).to.equal(5);

            expect(result[5].x).to.equal(4);
            expect(result[5].y).to.equal(6);

            expect(result[6].x).to.equal(4);
            expect(result[6].y).to.equal(7);
        });

        it("clips rectangle fully inside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(1, 6),
                new geometry.Point(7, 6)]);
            var rect = new geometry.Rectangle(3, 4, 3, 1);

            var result = poly.clip(rect);

            expect(result.length).to.equal(4);

            expect(result[0].x).to.equal(6);
            expect(result[0].y).to.equal(4);

            expect(result[1].x).to.equal(3);
            expect(result[1].y).to.equal(4);

            expect(result[2].x).to.equal(3);
            expect(result[2].y).to.equal(5);

            expect(result[3].x).to.equal(6);
            expect(result[3].y).to.equal(5);
        });

        it("tries to clip rectangle outside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(1, 6),
                new geometry.Point(7, 6)]);
            var rect = new geometry.Rectangle(-1, 1, 2, 1);

            var result = poly.clip(rect);

            expect(result.length).to.equal(0);
        });

        it("tries to clip rectangle outside polygon", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(5, 1),
                new geometry.Point(1, 6),
                new geometry.Point(7, 6)]);
            var rect = new geometry.Rectangle(7, 1, 3, 2);

            var result = poly.clip(rect);

            expect(result.length).to.equal(0);
        });

    });
});
