var geometry = require("../src/index");
var expect = require("chai").expect;

describe("Rectangle", function(){
    describe("new Rectangle", function(){
        it("creates rectangle object", function(){
            var rect = new geometry.Rectangle();

            expect(rect instanceof geometry.IShape).to.equal(true);
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var rect = new geometry.Rectangle();
            expect(rect.toString()).to.equal("[object Rectangle]");
        });
    });

    describe("constructor", function () {
        it("checks constructor property", function () {
            var rect = new geometry.Rectangle();
            expect(rect.constructor).to.equal(geometry.Rectangle);
        });
    });

    describe("new Rectangle", function () {
        it("creates rectangle object from properties", function () {
            var rect = new geometry.Rectangle(1, 2, 3, 4);

            expect(rect.x).to.equal(1);
            expect(rect.y).to.equal(2);
            expect(rect.width).to.equal(3);
            expect(rect.height).to.equal(4);
        });

        it("creates rectangle object from wrong properties", function () {
            var rect = new geometry.Rectangle(-3.14, -0, 0.9, 9.8);

            expect(rect.x).to.equal(-3);
            expect(rect.y).to.equal(0);
            expect(rect.width).to.equal(0);
            expect(rect.height).to.equal(9);
        });

        it("creates rectangle object from wrong properties", function () {
            var rect = new geometry.Rectangle(NaN, true, Infinity, "3.14");

            expect(rect.x).to.equal(0);
            expect(rect.y).to.equal(1);
            expect(rect.width).to.equal(0);
            expect(rect.height).to.equal(3);
        });

        it("creates rectangle object with negative width", function () {
            var rect = new geometry.Rectangle(0, 0, -1, 1);

            expect(rect.x).to.equal(-1);
            expect(rect.y).to.equal(0);
            expect(rect.width).to.equal(1);
            expect(rect.height).to.equal(1);
        });

        it("creates rectangle object with negative height", function () {
            var rect = new geometry.Rectangle(0, 0, 1, -1);

            expect(rect.x).to.equal(0);
            expect(rect.y).to.equal(-1);
            expect(rect.width).to.equal(1);
            expect(rect.height).to.equal(1);
        });

        it("creates rectangle object with negative size", function () {
            var rect = new geometry.Rectangle(0, 0, -2, -1);

            expect(rect.x).to.equal(-2);
            expect(rect.y).to.equal(-1);
            expect(rect.width).to.equal(2);
            expect(rect.height).to.equal(1);
        });
    });

    describe("contains()", function(){
        it("checks point to be inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);

            expect(r.contains(new geometry.Point(0, 0))).to.equal(true);
            expect(r.contains(new geometry.Point(8, 0))).to.equal(true);
            expect(r.contains(new geometry.Point(8, 8))).to.equal(true);
            expect(r.contains(new geometry.Point(0, 8))).to.equal(true);
            expect(r.contains(new geometry.Point(4, 4))).to.equal(true);
        });

        it("checks point to be inside empty rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 0, 0);

            expect(r.contains(new geometry.Point(0, 0))).to.equal(true);
        });

        it("checks each point inside rectangle", function(){
            var r = new geometry.Rectangle(-2, -1, 3, 2);

            expect(r.contains(new geometry.Point(-2, -1))).to.equal(true);
            expect(r.contains(new geometry.Point(-1, -1))).to.equal(true);
            expect(r.contains(new geometry.Point(0, -1))).to.equal(true);
            expect(r.contains(new geometry.Point(1, -1))).to.equal(true);

            expect(r.contains(new geometry.Point(-2, 0))).to.equal(true);
            expect(r.contains(new geometry.Point(-1, 0))).to.equal(true);
            expect(r.contains(new geometry.Point(0, 0))).to.equal(true);
            expect(r.contains(new geometry.Point(1, 0))).to.equal(true);

            expect(r.contains(new geometry.Point(-2, 1))).to.equal(true);
            expect(r.contains(new geometry.Point(-1, 1))).to.equal(true);
            expect(r.contains(new geometry.Point(0, 1))).to.equal(true);
            expect(r.contains(new geometry.Point(1, 1))).to.equal(true);
        });

        it("check point out of rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);

            expect(r.contains(new geometry.Point(0, 9))).to.equal(false);
            expect(r.contains(new geometry.Point(9, 8))).to.equal(false);
            expect(r.contains(new geometry.Point(-1, 0))).to.equal(false);
            expect(r.contains(new geometry.Point(4, 666))).to.equal(false);
        });

        it("check line inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(1, 1),
                new geometry.Point(5, 4));

            expect(r.contains(line)).to.equal(true);
        });

        it("check line outside rectangle (one point is outside)", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(-1, 1),
                new geometry.Point(5, 4));

            expect(r.contains(line)).to.equal(false);
        });

        it("check line outside rectangle (one point is outside)", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(1, 1),
                new geometry.Point(5, 9));

            expect(r.contains(line)).to.equal(false);
        });

        it("check line outside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(-1, -1),
                new geometry.Point(-5, -1));

            expect(r.contains(line)).to.equal(false);
        });

        it("check polygon inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(0, 8),
                new geometry.Point(8, 8)]);

            expect(r.contains(poly)).to.equal(true);
        });

        it("check polygon inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(0, 8),
                new geometry.Point(8, 8),
                new geometry.Point(4, 4),
                new geometry.Point(8, 0)
            ]);

            expect(r.contains(poly)).to.equal(true);
        });

        it("check polygon outside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var poly = new geometry.Polygon([
                new geometry.Point(2, 2),
                new geometry.Point(7, 3),
                new geometry.Point(5, 9)]);

            expect(r.contains(poly)).to.equal(false);
        });

        it("check rectangle inside rectangle", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(1, 1, 5, 5);

            expect(r1.contains(r2)).to.equal(true);
        });

        it("check rectangle inside rectangle", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(0, 0, 8, 8); // same size and position

            expect(r1.contains(r2)).to.equal(true);
        });

        it("check rectangle outside rectangle", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(-1, 0, 8, 8);

            expect(r1.contains(r2)).to.equal(false);
        });

        it("check rectangle outside rectangle", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(0, 0, 8, 9);

            expect(r1.contains(r2)).to.equal(false);
        });
    });

    describe("equals()", function(){
        it("checks two same rectangles", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(0, 0, 8, 8);

            expect(r1.equals(r2)).to.equal(true);
        });

        it("checks two different rectangles", function(){
            var r1 = new geometry.Rectangle(0, 0, 8, 8);
            var r2 = new geometry.Rectangle(1, 0, 8, 8);

            expect(r1.equals(r2)).to.equal(false);
        });
    });

    describe("intersects()", function(){
        it("checks point to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            // Should be same as contains()
            expect(r.intersects(new geometry.Point(1, 2))).to.equal(true);
            expect(r.intersects(new geometry.Point(0, 0))).to.equal(true);
            expect(r.intersects(new geometry.Point(1, 2))).to.equal(true);
            expect(r.intersects(new geometry.Point(3, 3))).to.equal(true);
            expect(r.intersects(new geometry.Point(4, 4))).to.equal(true);
        });

        it("checks point to not intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            // Should be same as contains()
            expect(r.intersects(new geometry.Point(0, 5))).to.equal(false);
            expect(r.intersects(new geometry.Point(5, 4))).to.equal(false);
            expect(r.intersects(new geometry.Point(-1, 0))).to.equal(false);
            expect(r.intersects(new geometry.Point(3, 666))).to.equal(false);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(2, 2),
                new geometry.Point(999999, 2)); // even so

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(-3, 2),
                new geometry.Point(999999, 1)); // even so

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(3, -2),
                new geometry.Point(1, 9)); // even so

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(-1, 0),
                new geometry.Point(5, 0));

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(4, -1),
                new geometry.Point(4, 5));

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(-1, 4),
                new geometry.Point(5, 4));

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(0, -1),
                new geometry.Point(0, 5));

            expect(r.intersects(l)).to.equal(true);
        });

        it("checks line to not intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var l = new geometry.Line(
                new geometry.Point(5, 2),
                new geometry.Point(5, 4));

            expect(r.intersects(l)).to.equal(false);
        });

        it("checks polygon to intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var p = new geometry.Polygon([
                new geometry.Point(2, 2),
                new geometry.Point(4, 4),
                new geometry.Point(-1, 5)]);

            expect(r.intersects(p)).to.equal(true);
        });

        it("checks polygon to be inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 6, 6);

            var p = new geometry.Polygon([
                new geometry.Point(1, 1),
                new geometry.Point(4, 4),
                new geometry.Point(0, 4)]);

            expect(r.intersects(p)).to.equal(true);
        });

        it("checks polygon to not intersect rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 4, 4);

            var p = new geometry.Polygon([
                new geometry.Point(-1, 0),
                new geometry.Point(-4, -4),
                new geometry.Point(-1, -5)]);

            expect(r.intersects(p)).to.equal(false);
        });

        it("checks intersected rectangles", function(){
            var r1 = new geometry.Rectangle(0, 0, 4, 4);
            var r2 = new geometry.Rectangle(0, 0, 4, 4);

            expect(r1.intersects(r2)).to.equal(true);

            r2 = new geometry.Rectangle(-1, -1, 2, 2);
            expect(r1.intersects(r2)).to.equal(true);

            r2 = new geometry.Rectangle(1, 1, 2, 2);
            expect(r1.intersects(r2)).to.equal(true);

            r2 = new geometry.Rectangle(3, 3, 2, 2);
            expect(r1.intersects(r2)).to.equal(true);
        });

        it("checks rectangle to be inside another one", function(){
            var r1 = new geometry.Rectangle(0, 0, 4, 4);
            var r2 = new geometry.Rectangle(1, 1, 2, 2);

            expect(r1.intersects(r2)).to.equal(true);
        });

        it("checks non-intersected rectangles", function(){
            var r1 = new geometry.Rectangle(0, 0, 4, 4);
            var r2 = new geometry.Rectangle(-2, -2, 2, 2);

            expect(r1.intersects(r2)).to.equal(false);

            r2 = new geometry.Rectangle(-3, -2, 2, 2);
            expect(r1.intersects(r2)).to.equal(false);

            r2 = new geometry.Rectangle(4, 4, 2, 2);
            expect(r1.intersects(r2)).to.equal(false);
        });
    });

    describe("clip()", function(){
        it("clips line fully inside rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(1, 1),
                new geometry.Point(5, 4));

            expect(r.clip(line)).to.equal(line);
        });

        it("clips line inside rectangle (diagonal)", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(-2, 9),
                new geometry.Point(9, -2));

            var result = r.clip(line);

            expect(result.a.x).to.equal(0);
            expect(result.a.y).to.equal(7);

            expect(result.b.x).to.equal(7);
            expect(result.b.y).to.equal(0);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips line inside rectangle (vertical)", function(){
            var r = new geometry.Rectangle(1, 2, 4, 4);
            var line = new geometry.Line(
                new geometry.Point(1, -1),
                new geometry.Point(1, 7));

            var result = r.clip(line);

            expect(result.a.x).to.equal(1);
            expect(result.a.y).to.equal(2);

            expect(result.b.x).to.equal(1);
            expect(result.b.y).to.equal(6);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips line inside rectangle (horizontal)", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(1, 3),
                new geometry.Point(8, 3));

            var result = r.clip(line);

            expect(result.a.x).to.equal(3);
            expect(result.a.y).to.equal(3);

            expect(result.b.x).to.equal(8);
            expect(result.b.y).to.equal(3);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips line (partially) inside rectangle (diagonal)", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(3, 2),
                new geometry.Point(7, 5));

            var result = r.clip(line);

            expect(result.a.x).to.equal(3);
            expect(result.a.y).to.equal(2);

            expect(result.b.x).to.equal(6);
            expect(result.b.y).to.equal(4);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips line inside rectangle (diagonal)", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(6, 1),
                new geometry.Point(5, 5));

            var result = r.clip(line);

            expect(result.a.x).to.equal(6);
            expect(result.a.y).to.equal(1);

            expect(result.b.x).to.equal(5);
            expect(result.b.y).to.equal(4);

            expect(r.contains(result)).to.equal(true);
        });

        it("tries to clip line outside of rectangle", function(){
            var r = new geometry.Rectangle(0, 0, 8, 8);
            var line = new geometry.Line(
                new geometry.Point(2, -1),
                new geometry.Point(11, -2));

            var result = r.clip(line);

            expect(result).to.equal(null);
        });

        it("tries to clip line outside of rectangle", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(9, 4),
                new geometry.Point(3, 7));

            var result = r.clip(line);

            expect(result).to.equal(null);
        });

        it("tries to clip line outside of rectangle", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(2, 0),
                new geometry.Point(2, 3));

            var result = r.clip(line);

            expect(result).to.equal(null);
        });

        it("tries to clip line outside of rectangle", function(){
            var r = new geometry.Rectangle(3, 1, 5, 3);
            var line = new geometry.Line(
                new geometry.Point(9, 1),
                new geometry.Point(9, 4));

            var result = r.clip(line);

            expect(result).to.equal(null);
        });

        it("clips rectangle fully inside rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(2, 1, 5, 5);

            var result = r1.clip(r2);

            expect(result).to.equal(r2);

            expect(r1.contains(result)).to.equal(true);
        });

        it("clips rectangle fully inside rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(3, 2, 2, 2);

            var result = r1.clip(r2);

            expect(result).to.equal(r2);

            expect(r1.contains(result)).to.equal(true);
        });

        it("clips itself", function(){
            var r = new geometry.Rectangle(2, 1, 5, 5);

            var result = r.clip(r);

            expect(result).to.equal(r);
        });

        it("clips rectangle which intersects other rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(3, 2, 5, 5);

            var result = r1.clip(r2);

            expect(result.x).to.equal(3);
            expect(result.y).to.equal(2);
            expect(result.width).to.equal(4);
            expect(result.height).to.equal(4);
        });

        it("clips rectangle which intersects other rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(1, -1, 3, 4);

            var result = r1.clip(r2);

            expect(result.x).to.equal(2);
            expect(result.y).to.equal(1);
            expect(result.width).to.equal(2);
            expect(result.height).to.equal(2);

            expect(r1.contains(result)).to.equal(true);
        });

        it("tries to clip rectangle outside other rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(1, -3, 3, 4);

            var result = r1.clip(r2);

            expect(result).to.equal(null);
        });

        it("tries to clip rectangle outside other rectangle", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(-8, -8, 3, 4);

            var result = r1.clip(r2);

            expect(result).to.equal(null);
        });

        it("tries to clip rectangle with empty size", function(){
            var r1 = new geometry.Rectangle(2, 1, 5, 5);
            var r2 = new geometry.Rectangle(3, 2, 0, 0);

            var result = r1.clip(r2);

            expect(result).to.equal(r2);
        });

        it("clips polygon inside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(1, 6),
                new geometry.Point(5, 2),
                new geometry.Point(6, 6)]);

            var result = r.clip(polygon);
            var p1 = result.vertices[0];
            var p2 = result.vertices[1];
            var p3 = result.vertices[2];
            var p4 = result.vertices[3];

            expect(result.vertices.length).to.equal(4);

            expect(p1.x).to.equal(3);
            expect(p1.y).to.equal(6);

            expect(p2.x).to.equal(3);
            expect(p2.y).to.equal(4);

            expect(p3.x).to.equal(5);
            expect(p3.y).to.equal(2);

            expect(p4.x).to.equal(6);
            expect(p4.y).to.equal(6);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips polygon inside rectangle", function(){
            var poly = new geometry.Polygon([
                new geometry.Point(3, 1),
                new geometry.Point(1, 5),
                new geometry.Point(6, 6)]);
            var rect = new geometry.Rectangle(1, 3, 5, 2);

            var result = rect.clip(poly).vertices;

            expect(result.length).to.equal(4);

            expect(result[0].x).to.equal(4);
            expect(result[0].y).to.equal(3);

            expect(result[1].x).to.equal(2);
            expect(result[1].y).to.equal(3);

            expect(result[2].x).to.equal(1);
            expect(result[2].y).to.equal(5);

            expect(result[3].x).to.equal(5);
            expect(result[3].y).to.equal(5);
        });

        it("clips polygon inside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(3, 4),
                new geometry.Point(8, -2),
                new geometry.Point(8, 8),
                new geometry.Point(3, 8)]);

            var result = r.clip(polygon);
            var p1 = result.vertices[0];
            var p2 = result.vertices[1];
            var p3 = result.vertices[2];
            var p4 = result.vertices[3];
            var p5 = result.vertices[4];

            expect(result.vertices.length).to.equal(5);

            expect(p1.x).to.equal(7);
            expect(p1.y).to.equal(7);

            expect(p2.x).to.equal(3);
            expect(p2.y).to.equal(7);

            expect(p3.x).to.equal(3);
            expect(p3.y).to.equal(4);

            expect(p4.x).to.equal(4);
            expect(p4.y).to.equal(2);

            expect(p5.x).to.equal(7);
            expect(p5.y).to.equal(2);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips polygon inside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(5, 4),
                new geometry.Point(8, 1),
                new geometry.Point(8, 7)]);

            var result = r.clip(polygon);
            var p1 = result.vertices[0];
            var p2 = result.vertices[1];
            var p3 = result.vertices[2];

            expect(result.vertices.length).to.equal(3);

            expect(p1.x).to.equal(7);
            expect(p1.y).to.equal(6);

            expect(p2.x).to.equal(5);
            expect(p2.y).to.equal(4);

            expect(p3.x).to.equal(7);
            expect(p3.y).to.equal(2);

            expect(r.contains(result)).to.equal(true);
        });

        it("clips polygon inside rectangle (all polygon vertices outside rectangle, but shapes still intersected)", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(1, 4),
                new geometry.Point(8, 2),
                new geometry.Point(8, 6)]);

            var result = r.clip(polygon);
            var p1 = result.vertices[0];
            var p2 = result.vertices[1];
            var p3 = result.vertices[2];
            var p4 = result.vertices[3];

            expect(result.vertices.length).to.equal(4);

            expect(p1.x).to.equal(7);
            expect(p1.y).to.equal(5);

            expect(p2.x).to.equal(3);
            expect(p2.y).to.equal(4);

            expect(p3.x).to.equal(3);
            expect(p3.y).to.equal(3);

            expect(p4.x).to.equal(7);
            expect(p4.y).to.equal(2);
        });

        it("clips polygon inside rectangle", function(){
            var r = new geometry.Rectangle(4, 1, 5, 8);
            var polygon = new geometry.Polygon([
                new geometry.Point(8, 2),
                new geometry.Point(1, 2),
                new geometry.Point(1, 8),
                new geometry.Point(8, 8),
                new geometry.Point(3, 5)
            ]);

            var result = r.clip(polygon);
            var p1 = result.vertices[0];
            var p2 = result.vertices[1];
            var p3 = result.vertices[2];
            var p4 = result.vertices[3];
            var p5 = result.vertices[4];
            var p6 = result.vertices[5];

            expect(result.vertices.length).to.equal(6);

            expect(p1.x).to.equal(4);
            expect(p1.y).to.equal(5);

            expect(p2.x).to.equal(4);
            expect(p2.y).to.equal(4);

            expect(p3.x).to.equal(8);
            expect(p3.y).to.equal(2);

            expect(p4.x).to.equal(4);
            expect(p4.y).to.equal(2);

            expect(p5.x).to.equal(4);
            expect(p5.y).to.equal(8);

            expect(p6.x).to.equal(8);
            expect(p6.y).to.equal(8);

            expect(r.contains(result)).to.equal(true);
        });

        it("tries to clip polygon outside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(0, -1),
                new geometry.Point(2, 1),
                new geometry.Point(-1, 2)]);

            var result = r.clip(polygon);

            expect(result).to.equal(null);
        });

        it("tries to clip polygon outside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(11, 4),
                new geometry.Point(8, 1),
                new geometry.Point(8, 7)]);

            var result = r.clip(polygon);

            expect(result).to.equal(null);
        });

        it("tries to clip polygon outside rectangle", function(){
            var r = new geometry.Rectangle(3, 2, 4, 5);
            var polygon = new geometry.Polygon([
                new geometry.Point(4, 8),
                new geometry.Point(1, 9),
                new geometry.Point(3, 10),
                new geometry.Point(9, 8)]);

            var result = r.clip(polygon);

            expect(result).to.equal(null);
        });

    });
});
