var geometry = require("../src/index");
var expect = require("chai").expect;

describe("Line", function(){
    describe("new Line", function(){
        it("creates Line object", function(){
            var line = new geometry.Line(null, null);
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var line = new geometry.Line(null, null);
            expect(line.toString()).to.equal("[object Line]");
        });
    });

    describe("constructor", function () {
        it("checks constructor property", function () {
            var line = new geometry.Line(null, null);
            expect(line.constructor).to.equal(geometry.Line);
        });
    });

    describe("new Line", function(){
        it("creates line object from properties", function(){
            var a = new geometry.Point(0, 1);
            var b = new geometry.Point(2, 3);
            var line = new geometry.Line(a, b);

            expect(line.a).to.equal(a);
            expect(line.b).to.equal(b);
        });
    });

    describe("contains()", function(){
        it("checks point to be on line (diagonal)", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            expect(line.contains(new geometry.Point(0, 0))).to.equal(true);
            expect(line.contains(new geometry.Point(1, 1))).to.equal(true);
            expect(line.contains(new geometry.Point(2, 2))).to.equal(true);
            expect(line.contains(new geometry.Point(3, 3))).to.equal(true);
        });

        it("checks point to be outside of line (diagonal)", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            expect(line.contains(new geometry.Point(-1, 0))).to.equal(false);
            expect(line.contains(new geometry.Point(2, 1))).to.equal(false);
            expect(line.contains(new geometry.Point(1, 2))).to.equal(false);
            expect(line.contains(new geometry.Point(4, 3))).to.equal(false);
        });

        it("checks point to be on line (y-axis)", function(){
            var a = new geometry.Point(3, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            expect(line.contains(new geometry.Point(3, 0))).to.equal(true);
            expect(line.contains(new geometry.Point(3, 1))).to.equal(true);
            expect(line.contains(new geometry.Point(3, 2))).to.equal(true);
            expect(line.contains(new geometry.Point(3, 3))).to.equal(true);
        });

        it("checks point to be on line (x-axis)", function(){
            var a = new geometry.Point(0, 3);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            expect(line.contains(new geometry.Point(0, 3))).to.equal(true);
            expect(line.contains(new geometry.Point(1, 3))).to.equal(true);
            expect(line.contains(new geometry.Point(2, 3))).to.equal(true);
            expect(line.contains(new geometry.Point(3, 3))).to.equal(true);
        });

        it("checks line to be on line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 1);
            var b2 = new geometry.Point(2, 2);
            var line2 = new geometry.Line(a2, b2); // collinear

            expect(line1.contains(line1)).to.equal(true); // should be ok
            expect(line1.contains(line2)).to.equal(true);
        });

        it("checks line to be on line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(0, 0);
            var b2 = new geometry.Point(2, 2);
            var line2 = new geometry.Line(a2, b2); // collinear

            expect(line1.contains(line2)).to.equal(true);
        });

        it("checks line to be on line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(2, 2);
            var b2 = new geometry.Point(3, 3);
            var line2 = new geometry.Line(a2, b2); // collinear

            expect(line1.contains(line2)).to.equal(true);
        });

        it("checks line to be on line", function(){
            var a1 = new geometry.Point(0, 3);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 3);
            var b2 = new geometry.Point(3, 3);
            var line2 = new geometry.Line(a2, b2); // collinear

            expect(line1.contains(line2)).to.equal(true);
        });

        it("checks line to be on line", function(){
            var a1 = new geometry.Point(3, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(3, 3);
            var b2 = new geometry.Point(3, 1);
            var line2 = new geometry.Line(a2, b2); // collinear

            expect(line1.contains(line2)).to.equal(true);
        });

        it("checks line to be outside line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(0, 1);
            var b2 = new geometry.Point(3, 4);
            var line2 = new geometry.Line(a2, b2); // parallel

            expect(line1.contains(line2)).to.equal(false);
        });

        it("checks line to be outside line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(0, 3);
            var b2 = new geometry.Point(3, 0);
            var line2 = new geometry.Line(a2, b2); // cross

            expect(line1.contains(line2)).to.equal(false);
        });
    });

    describe("equals()", function(){
        it("checks two same lines", function(){
            var a1 = new geometry.Point(1, 2);
            var b1 = new geometry.Point(3, 4);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 2);
            var b2 = new geometry.Point(3, 4);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.equals(line2)).to.equal(true);
        });

        it("checks two different lines", function(){
            var a1 = new geometry.Point(1, 2);
            var b1 = new geometry.Point(3, 4);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(2, 2);
            var b2 = new geometry.Point(3, 4);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.equals(line2)).to.equal(false);
        });
    });

    describe("intersects()", function(){
        it("checks point to intersect the line (diagonal)", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            // Should be same as contains()
            expect(line.intersects(new geometry.Point(0, 0))).to.equal(true);
            expect(line.intersects(new geometry.Point(1, 1))).to.equal(true);
            expect(line.intersects(new geometry.Point(2, 2))).to.equal(true);
            expect(line.intersects(new geometry.Point(3, 3))).to.equal(true);
        });

        it("checks point to not intersect the line (diagonal)", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            // Should be same as contains()
            expect(line.intersects(new geometry.Point(1, 0))).to.equal(false);
            expect(line.intersects(new geometry.Point(1, -1))).to.equal(false);
            expect(line.intersects(new geometry.Point(2, 4))).to.equal(false);
            expect(line.intersects(new geometry.Point(5, 3))).to.equal(false)
        });

        it("checks line to intersect line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 1);
            var b2 = new geometry.Point(2, 2);
            var line2 = new geometry.Line(a2, b2);

            // Lines are collinear, so considered as non-intersected!
            expect(line1.intersects(line2)).to.equal(false);
        });

        it("checks line to intersects line (cross)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 5);
            var b2 = new geometry.Point(5, 1);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(true);
        });

        it("checks line to intersects line (cross)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(0, 3);
            var b2 = new geometry.Point(3, 0);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(true);
        });

        it("checks line to not intersects line (cross)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(-1, -1);
            var b2 = new geometry.Point(5, -1);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(false);
        });

        it("checks line to not intersects line (cross)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(-1, 4);
            var b2 = new geometry.Point(5, 4);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(false);
        });

        it("checks line to not intersects line (cross)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(2, -2);
            var b2 = new geometry.Point(-1, 1);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(true);
        });

        it("checks line to intersect line", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(-1, 1);
            var b2 = new geometry.Point(2, 1);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(true);
        });

        it("checks line to not intersect line (parallels)", function(){
            var a1 = new geometry.Point(0, 0);
            var b1 = new geometry.Point(3, 3);
            var line1 = new geometry.Line(a1, b1);

            var a2 = new geometry.Point(1, 0);
            var b2 = new geometry.Point(4, 3);
            var line2 = new geometry.Line(a2, b2);

            expect(line1.intersects(line2)).to.equal(false);
        });

        it("checks line to intersects polygon", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(4, 4);
            var line = new geometry.Line(a, b);

            var poly = new geometry.Polygon([
                new geometry.Point(0, 1),
                new geometry.Point(-1, 4),
                new geometry.Point(5, 0)
            ]);

            expect(line.intersects(poly)).to.equal(true);
        });

        it("checks line to intersects polygon", function(){
            var a = new geometry.Point(0, 4);
            var b = new geometry.Point(4, 4);
            var line = new geometry.Line(a, b);

            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(4, 4),
                new geometry.Point(8, 0)
            ]);

            expect(line.intersects(poly)).to.equal(true);
        });

        it("checks line to intersects polygon", function(){
            var a = new geometry.Point(8, -1);
            var b = new geometry.Point(8, 4);
            var line = new geometry.Line(a, b);

            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(4, 4),
                new geometry.Point(8, 0)
            ]);

            expect(line.intersects(poly)).to.equal(true);
        });

        it("checks line to be inside polygon", function(){
            var a = new geometry.Point(1, 1);
            var b = new geometry.Point(4, 4);
            var line = new geometry.Line(a, b);

            var poly = new geometry.Polygon([
                new geometry.Point(0, 0),
                new geometry.Point(6, 6),
                new geometry.Point(6, 0)
            ]);

            expect(line.intersects(poly)).to.equal(true);
        });

        it("checks line to not intersects polygon", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(4, 4);
            var line = new geometry.Line(a, b);

            var poly = new geometry.Polygon([
                new geometry.Point(0, 1),
                new geometry.Point(-1, 4),
                new geometry.Point(4, 5)
            ]);

            expect(line.intersects(poly)).to.equal(false);
        });

        it("checks line to intersects rectangle", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(4, 5);
            var line = new geometry.Line(a, b);

            var rect = new geometry.Rectangle(0, 1, 4, 3);

            expect(line.intersects(rect)).to.equal(true);
        });

        it("checks line to intersects rectangle (line is fully inside and don't collide with rect edges)", function(){
            var a = new geometry.Point(1, 1);
            var b = new geometry.Point(2, 3);
            var line = new geometry.Line(a, b);

            var rect = new geometry.Rectangle(0, 0, 6, 5);

            expect(line.intersects(rect)).to.equal(true);
        });

        it("checks line to not intersect rectangle", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(8, 1);
            var line = new geometry.Line(a, b);

            var rect = new geometry.Rectangle(0, 1, 4, 3);

            expect(line.intersects(rect)).to.equal(false);
        });
    });

    describe("makeRectangle()", function(){
        it("creates a new rectangle instance", function(){
            var a = new geometry.Point(0, 0);
            var b = new geometry.Point(3, 3);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result.x).to.equal(0);
            expect(result.y).to.equal(0);
            expect(result.width).to.equal(3);
            expect(result.height).to.equal(3);
        });

        it("creates a new rectangle instance", function(){
            var a = new geometry.Point(2, 2);
            var b = new geometry.Point(5, 3);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result.x).to.equal(2);
            expect(result.y).to.equal(2);
            expect(result.width).to.equal(3);
            expect(result.height).to.equal(1);
        });

        it("creates a new rectangle instance", function(){
            var a = new geometry.Point(5, 3);
            var b = new geometry.Point(2, 2);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result.x).to.equal(2);
            expect(result.y).to.equal(2);
            expect(result.width).to.equal(3);
            expect(result.height).to.equal(1);
        });

        it("creates a new rectangle instance", function(){
            var a = new geometry.Point(-1, -1);
            var b = new geometry.Point(0, 1);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result.x).to.equal(-1);
            expect(result.y).to.equal(-1);
            expect(result.width).to.equal(1);
            expect(result.height).to.equal(2);
        });

        it("creates a new rectangle instance", function(){
            var a = new geometry.Point(0, 1);
            var b = new geometry.Point(-1, -1);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result.x).to.equal(-1);
            expect(result.y).to.equal(-1);
            expect(result.width).to.equal(1);
            expect(result.height).to.equal(2);
        });

        it("tries to create rectangle from line with same a and b", function(){
            var a = new geometry.Point(2, 3);
            var b = new geometry.Point(2, 3);
            var line = new geometry.Line(a, b);

            var result = line.makeRectangle();

            expect(result).to.equal(null);
        });

        it("tries to create rectangle from \"thin\" line (y-axis parallel)", function(){
            var a = new geometry.Point(2, 3);
            var b = new geometry.Point(4, 3);
            var line = new geometry.Line(a, b); // y-axis parallel

            var result = line.makeRectangle();

            expect(result).to.equal(null);
        });

        it("tries to create rectangle from \"thin\" line (x-axis parallel)", function(){
            var a = new geometry.Point(2, 4);
            var b = new geometry.Point(2, 3);
            var line = new geometry.Line(a, b); // x-axis parallel

            var result = line.makeRectangle();

            expect(result).to.equal(null);
        });
    });
    
});
