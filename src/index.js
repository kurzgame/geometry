/**
 * Geometry module entry point.
 *
 * @module geometry
 * @main geometry
 */
module.exports = Object.freeze({
    "Line": require("Line"),
    "Point": require("Point"),
    "IShape": require("IShape"),
    "Polygon": require("Polygon"),
    "Rectangle": require("Rectangle")
});
