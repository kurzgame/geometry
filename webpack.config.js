"use strict";

var webpack = require("webpack");

module.exports = {
    "entry": "./src",
    "module": {
        "loaders": [
            {
                "loader": "strict-loader"
            }
        ]
    },
    "output": {
        "filename": "geometry.js",
        "library": "geometry"
    },
    "watch": true,
    "watchOptions": {
        "aggregateTimeout": 100
    },
	"plugins": [
		new webpack.optimize.UglifyJsPlugin({
			"compress": {
				"warnings": false
			}
		})
	]
};
